﻿using System;
using System.Configuration;
using MimeKit;
using MailKit.Net.Smtp;
using System.IO;

namespace BackupWatchApp
{
    class Program
    {
        //全局变量
        static string title = "BackUp Watch(Do not close this program,data transfer will be aborted.)";

        
        static void Main(string[] args)
        {
            System.Console.Title = title;
            System.Console.Beep();
            System.Console.ForegroundColor = ConsoleColor.Green;
            System.Console.WriteLine("BackUp Watch is running......");
            System.Console.WriteLine("***********************************************************");

            
            string exeTime = ConfigurationManager.AppSettings["SYNC_TIME"];
            string path = ConfigurationManager.AppSettings["BACK_PATH"];
            while (true)
            {
                try
                {
                    string timeNow = DateTime.Now.ToString("HH:mm:ss");

                    System.Console.Title = title + timeNow;

                    System.Threading.Thread.Sleep(1000);
                    //定时发送backup Mail
                    if (timeNow.StartsWith(exeTime))
                    {
                        System.Console.WriteLine("获取文件");
                        string fileWQ = "WQ_" + DateTime.Now.ToString("yyyyMMdd") + ".rar";
                        if (System.IO.File.Exists(path + fileWQ))
                        {
                            System.Console.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "    Start Backup Files " + fileWQ);

                            string subject = "【Backup】-WQ_" + DateTime.Now.ToString("yyyyMMdd");

                            SendMailKit(subject, fileWQ + " bak", fileWQ);
                            
                            System.Console.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "    Backup Success");
                        }

                        string fileZL = "ZL_" + DateTime.Now.ToString("yyyyMMdd") + ".rar";
                        if (System.IO.File.Exists(path + fileZL))
                        {
                            System.Console.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "    Start Backup Files " + fileZL);
                            //参数：接收者邮箱、内容
                            string subject = "【Backup】-ZL_" + DateTime.Now.ToString("yyyyMMdd");

                            SendMailKit(subject, fileZL + " bak", fileZL);

                            System.Console.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "    Backup Success");
                        }

                        System.Threading.Thread.Sleep(2000 * 60);
                    }
                }
                catch (Exception ex)
                {
                    System.Console.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "    " + ex.Message);
                    System.Threading.Thread.Sleep(1000);
                }
            }
        }

        private static void SendMailKit(string subject, string body, string attName)
        {
            var message = new MimeMessage();
            message.From.Add(new MailboxAddress("AvStyle", ConfigurationManager.AppSettings["MAIL_FROM_ADDRESS"]));
            message.To.Add(new MailboxAddress("Sam", ConfigurationManager.AppSettings["MAIL_TO_ADDRESS"]));
            message.Subject = subject;

            /*var plain = new TextPart("plain")
            {
                Text = @"不好意思，我在测试程序，刚才把QQ号写错了，Sorry！"
            };*/
            var html = new TextPart("html")
            {
                Text = body
            };

            string path = ConfigurationManager.AppSettings["BACK_PATH"];
            // create an image attachment for the file located at path
            var attachment = new MimePart("bak", "rar")
            {
                ContentObject = new ContentObject(File.OpenRead(path + attName), ContentEncoding.Default),
                ContentDisposition = new ContentDisposition(ContentDisposition.Attachment),
                ContentTransferEncoding = ContentEncoding.Base64,
                FileName = Path.GetFileName(path+ attName)
            };

            var alternative = new Multipart("alternative");
            //alternative.Add(plain);
            alternative.Add(html);

            // now create the multipart/mixed container to hold the message text and the
            // image attachment
            var multipart = new Multipart("mixed");
            multipart.Add(alternative);
            multipart.Add(attachment);
         
            message.Body = multipart;

            using (var client = new MailKit.Net.Smtp.SmtpClient())
            {
                client.Connect(ConfigurationManager.AppSettings["MAIL_HOST"],Convert.ToInt32(ConfigurationManager.AppSettings["MAIL_PORT"]), Convert.ToBoolean(ConfigurationManager.AppSettings["MAIL_ENABLESSL"]));

                // Note: since we don't have an OAuth2 token, disable
                // the XOAUTH2 authentication mechanism.
                client.AuthenticationMechanisms.Remove("XOAUTH2");

                // Note: only needed if the SMTP server requires authentication
                client.Authenticate(ConfigurationManager.AppSettings["MAIL_FROM_ADDRESS"], ConfigurationManager.AppSettings["MAIL_PASSWORD"]);

                client.Send(message);
                client.Disconnect(true);
            }
        }
    }
}
